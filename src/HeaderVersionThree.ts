import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { IHeaderInfo3, IVersionSpecs, VersionSpecificationAbstract } from './VersionSpecificationsAbstract';

const specs: IVersionSpecs = {
  extraPos: 3,
  separators: {
    header: '|',
    oid: '_',
  },
  sniffer: new RegExp(
    /(^[a-zA-Z][a-z]|[A-Z][A-Z])_([a-z]{2}\.|[a-z]{3}|[A-Z][a-z]{2}\.|\[[A-z][a-z]|'[A-Z][a-z]|[A-Z][a-z][a-z])\|[A-Z]{3}_[0-9]{5,10}\.[0-9]{1,2}-.*/,
  ),
  version: 3,
};

class HeaderVersionThree extends VersionSpecificationAbstract {
  protected logLevel: LogLevelDescType;
  protected logging: Logger;

  constructor(logLevel: LogLevelDescType = 'info') {
    super(specs);
    this.logLevel = logLevel;
    this.logging = new Logger(logLevel);
  }

  public parse(header: string): IHeaderInfo3 {
    const log = this.logging.getLogger('HeaderVersionThree::parse');
    const details: IHeaderInfo3 = {
      e: [] as string[],
      g: '',
      gv: '',
      l: '',
      mistInfo: {
        stable_id: '',
      },
      s: '',
    };
    const fields = header.split(this.separators.header);
    log.debug(fields);
    const orgID = fields[0];
    const genReg = new RegExp(/.{2}/);
    log.debug(`orgID parsed: ${orgID}`);

    const genRegMatch = orgID.match(genReg);
    if (!genRegMatch) {
      throw new Error(`Genus does not match version ${this.version} definition`);
    }
    details.g = genRegMatch[0];
    log.debug(`genusHumanId: ${details.g}`);

    const speReg = new RegExp('\\' + this.separators.oid + '.{3}');
    const speRegMatch = orgID.match(speReg);
    if (!speRegMatch) {
      throw new Error(`Species does not match version ${this.version} definition`);
    }
    details.s = speRegMatch[0].slice(1);
    log.debug(`speciesHumanId: ${details.s}`);

    details.mistInfo.stable_id = fields[1];
    const versionAndLocus = fields[1].split('-');
    details.gv = versionAndLocus.splice(0, 1).join('-');
    details.l = versionAndLocus.join('-');
    details.e = fields.slice(this.extraPos);

    return details;
  }

  public write(details: IHeaderInfo3): string {
    const oid = [details.g, details.s].join(this.separators.oid);
    return [oid, details.mistInfo.stable_id].concat(details.e).join(this.separators.header);
  }
}

export { HeaderVersionThree };
