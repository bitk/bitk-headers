interface IGene {
  stable_id: string;
  [other: string]: any;
}

interface ISeparators {
  oid: string;
  header: string;
}

interface IHeaderInfo {
  g: string;
  s: string;
  gid: string;
  l: string;
  a: string;
  e: string[];
  mistInfo: IGene;
}

interface IHeaderInfo3 {
  g: string;
  s: string;
  l: string;
  gv: string; // genome version
  e: string[];
  mistInfo: IGene;
}

interface IVersionSpecs {
  extraPos: number;
  version: number;
  sniffer: RegExp;
  separators: ISeparators;
}

abstract class VersionSpecificationAbstract {
  public readonly sniffer: RegExp;
  public readonly version: number;
  protected readonly separators: ISeparators;
  protected readonly extraPos: number;

  constructor(versionSpecs: IVersionSpecs) {
    this.version = versionSpecs.version;
    this.sniffer = versionSpecs.sniffer;
    this.separators = versionSpecs.separators;
    this.extraPos = versionSpecs.extraPos;
  }

  public abstract parse(header: string): IHeaderInfo | IHeaderInfo3;
  public abstract write(indo: IHeaderInfo | IHeaderInfo3): string;
}

export { IHeaderInfo, IHeaderInfo3, IVersionSpecs, VersionSpecificationAbstract };
