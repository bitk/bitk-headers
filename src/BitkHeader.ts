import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';

import { Genes } from 'mist3-ts';
import { HeaderVersionThree } from './HeaderVersionThree';
import { IHeaderInfo3, VersionSpecificationAbstract } from './VersionSpecificationsAbstract';

const supportedVersions = [HeaderVersionThree];

class BitkHeader {
  protected originalHeader: undefined | string;
  protected originalVersion: undefined | number;
  protected details: IHeaderInfo3;
  protected logging: Logger;
  protected logLevel: LogLevelDescType;

  constructor(logLevel: LogLevelDescType = 'info') {
    this.originalHeader = undefined;
    this.originalVersion = undefined;
    this.details = {
      e: [],
      g: '',
      gv: '',
      l: '',
      mistInfo: {
        stable_id: '',
      },
      s: '',
    };
    this.logLevel = logLevel;
    this.logging = new Logger(logLevel);
  }

  /**
   * Parse header string to build a BitkHeader. This method can be chained.
   *
   * @param {boolean} [options={skip: false}]
   * @returns {BitkHeader}
   * @memberof BitkHeader
   */
  public parse(header: string, options: { skip: boolean } = { skip: false }): BitkHeader {
    const log = this.logging.getLogger('BitkHeaders::parse');
    this.originalHeader = header;
    this.originalVersion = this.sniffVersion();
    log.info(`Header ${this.originalHeader} seems to be of version ${this.originalVersion}`);
    if (!this.originalVersion && !options.skip) {
      log.error(`Invalid header: ${this.originalHeader}`);
      throw Error(`Invalid header: ${this.originalHeader}`);
    } else {
      const versionSpecs = this.getVersionSpecs(this.originalVersion as number);
      this.details = versionSpecs.parse(this.originalHeader) as IHeaderInfo3;
    }
    return this;
  }

  /**
   * Return a JSON object with all the info in the BitkHeader object.
   *
   * @returns {IHeaderInfo3}
   * @memberof BitkHeader
   */
  public getInfo(): IHeaderInfo3 {
    return this.details;
  }

  /**
   * Return the locus of the header
   *
   * @returns {string}
   * @memberof BitkHeader
   */
  public getLocus(): string {
    return this.details.l;
  }

  /**
   * Returns the genome version if it knows. Otherwise returns null.
   *
   * @returns
   * @memberof BitkHeader
   */
  public getGenomeVersion() {
    if (this.hasGeneInfo()) {
      return this.details.mistInfo.stable_id.split('-')[0];
    }
    return null;
  }

  /**
   * Returns the MiST stable_id
   *
   * @returns {string}
   * @memberof BitkHeader
   */
  public getStableId(): string {
    return this.details.mistInfo.stable_id;
  }

  /**
   * Appends extra information to header object
   *
   * @param {string[]} extra
   * @returns {BitkHeader}
   * @memberof BitkHeader
   */
  public addExtra(extra: string[]): BitkHeader {
    this.details.e = this.details.e.concat(extra);
    return this;
  }

  /**
   * Fetch gene information from MiST3 API. This method can be chained.
   *
   * @param {{fetch: boolean, pass: boolean}} [options={fetch: true, pass: true}]
   * @returns
   * @memberof BitkHeader
   */
  public fetchGeneInfo(options: { fetch?: boolean; pass?: boolean } = { fetch: true, pass: true }) {
    const log = this.logging.getLogger('BitkHeaders::fetchGeneInfo');
    return new Promise((resolve, reject) => {
      if (this.hasGeneInfo()) {
        resolve();
      } else if (options.fetch) {
        log.warn(`Header ${this.originalHeader} does not have gene info. Fetching...`);
        const genes = new Genes(this.logLevel);
        genes.fetchByStableId(this.getStableId()).then(info => {
          log.debug(info);
          if (info.length === 0) {
            log.error(`No data recovered from ${this.getLocus()}`);
            reject(`No data recovered from ${this.getLocus()}`);
          } else {
            log.debug(`Found gene's stable_id: ${info.stable_id}`);
          }
          this.details.mistInfo = info;
          resolve();
          return;
        });
      } else if (options.pass) {
        log.warn(`Header ${this.originalHeader} does not have genome information on MiST3`);
        resolve();
      } else {
        log.error(`Genome info not found in header ${this.originalHeader}. No fetch allowed.`);
        reject(Error(`Genome info not found in header ${this.originalHeader}. No fetch allowed.`));
      }
    });
  }

  /**
   * Returns header at appropriate version
   *
   * @param {number} version
   * @returns
   * @memberof BitkHeader
   */
  public write(version: number) {
    const headerSpecs = this.getVersionSpecs(version);
    return headerSpecs.write(this.details);
  }

  /**
   * Returns a boolean to indicate if the gene info exists or not.
   *
   * @protected
   * @returns {boolean}
   * @memberof BitkHeader
   */
  protected hasGeneInfo(): boolean {
    const id = this.details.mistInfo.id;
    return typeof id === 'number';
  }

  /**
   * Return an instance of the HeaderVersion of a given version
   *
   * @protected
   * @param {number} version
   * @returns {VersionSpecificationAbstract}
   * @memberof BitkHeader
   */
  protected getVersionSpecs(version: number): VersionSpecificationAbstract {
    const versionSpecs = supportedVersions.find(supportedVersion => {
      const headeVersion = new supportedVersion();
      return headeVersion.version === version;
    });
    if (!versionSpecs) {
      throw new Error('Version ${version} has no specifications defined');
    }
    return new versionSpecs();
  }

  /**
   * Returns a guess of what version the original header is.
   *
   * @protected
   * @returns
   * @memberof BitkHeader
   */
  protected sniffVersion() {
    const log = this.logging.getLogger('BitkHeader::sniffVersion');
    const matches: number[] = [];
    const originalHeader = this.originalHeader as string;
    supportedVersions.forEach(versionSpecs => {
      const headerSpec = new versionSpecs(this.logLevel);
      const snifMatch = originalHeader.match(headerSpec.sniffer);
      if (snifMatch) {
        log.debug(snifMatch);
        matches.push(headerSpec.version);
      }
    });
    if (matches.length) {
      return Math.max(...matches);
    }
  }
}

export { BitkHeader };
