import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';

import { Genes } from 'mist3-ts';
import { HeaderVersionOne } from './HeaderVersionOne';
import { HeaderVersionTwo } from './HeaderVersionTwo';
import { IHeaderInfo, VersionSpecificationAbstract } from './VersionSpecificationsAbstract';

const supportedVersions = [HeaderVersionOne, HeaderVersionTwo];

class BitkHeaderLegacy {
  protected originalHeader: undefined | string;
  protected originalVersion: undefined | number;
  protected info: IHeaderInfo;
  protected logging: Logger;
  protected logLevel: LogLevelDescType;

  constructor(logLevel: LogLevelDescType = 'info') {
    this.originalHeader = undefined;
    this.originalVersion = undefined;
    this.info = {
      a: '',
      e: [],
      g: '',
      gid: '',
      l: '',
      mistInfo: {
        stable_id: '',
      },
      s: '',
    };
    this.logLevel = logLevel;
    this.logging = new Logger(logLevel);
  }

  /**
   * Parse header string to build a BitkHeaderLegacy. This method can be chained.
   *
   * @param {boolean} [options={skip: false}]
   * @returns {BitkHeaderLegacy}
   * @memberof BitkHeaderLegacy
   */
  public parse(header: string, options = { skip: false }): BitkHeaderLegacy {
    const log = this.logging.getLogger('BitkHeaderLegacys::parse');
    this.originalHeader = header;
    this.originalVersion = this.sniffVersion();
    log.info(`Header ${this.originalHeader} seems to be of version ${this.originalVersion}`);
    if (!this.originalVersion && !options.skip) {
      log.error(`Invalid header: ${this.originalHeader}`);
      throw Error(`Invalid header: ${this.originalHeader}`);
    } else {
      const versionSpecs = this.getVersionSpecs(this.originalVersion as number);
      this.info = versionSpecs.parse(this.originalHeader) as IHeaderInfo;
    }
    return this;
  }

  /**
   * Return a JSON object with all the info in the BitkHeaderLegacy object.
   *
   * @returns {IHeaderInfo}
   * @memberof BitkHeaderLegacy
   */
  public getInfo(): IHeaderInfo {
    return this.info;
  }

  /**
   * Return the locus of the header
   *
   * @returns {string}
   * @memberof BitkHeaderLegacy
   */
  public getLocus(): string {
    return this.info.l;
  }

  /**
   * Returns the genome version if it knows. Otherwise returns null.
   *
   * @returns
   * @memberof BitkHeaderLegacy
   */
  public getGenomeVersion() {
    if (this.hasGeneInfo()) {
      return this.info.mistInfo.stable_id.split('-')[0];
    }
    return null;
  }

  /**
   * Returns the MiST stable_id
   *
   * @returns {string}
   * @memberof BitkHeaderLegacy
   */
  public getStableId(): string {
    return this.info.mistInfo.stable_id;
  }

  /**
   * Appends extra information to header object
   *
   * @param {string[]} extra
   * @returns {BitkHeaderLegacy}
   * @memberof BitkHeaderLegacy
   */
  public addExtra(extra: string[]): BitkHeaderLegacy {
    this.info.e = this.info.e.concat(extra);
    return this;
  }

  /**
   * Fetch gene information from MiST3 API. This method can be chained.
   *
   * @param {{fetch: boolean, pass: boolean}} [options={fetch: true, pass: true}]
   * @returns
   * @memberof BitkHeaderLegacy
   */
  public fetchGeneInfo(options: { fetch?: boolean; pass?: boolean } = { fetch: true, pass: true }) {
    const log = this.logging.getLogger('BitkHeaderLegacys::fetchGeneInfo');
    return new Promise((resolve, reject) => {
      if (this.hasGeneInfo()) {
        resolve();
      } else if (options.fetch) {
        log.warn(`Header ${this.getLocus()} does not have gene info. Fetching...`);
        const genes = new Genes(this.logLevel);
        genes.fetchByAnyField(this.getLocus()).then(info => {
          log.debug(info);
          if (info.length > 1) {
            log.error(`Ambiguous data recovered from ${this.getLocus()}`);
            reject(`Ambiguous data recovered from ${this.getLocus()}`);
          } else if (info.length === 0) {
            log.error(`No data recovered from ${this.getLocus()}`);
            reject(`No data recovered from ${this.getLocus()}`);
          } else {
            log.debug(`Found gene's stable_id: ${info.stable_id}`);
          }
          this.info.mistInfo = info[0];
          resolve();
          return;
        });
      } else if (options.pass) {
        log.warn(`Header ${this.getLocus()} does not have genome information on MiST3`);
        resolve();
      } else {
        log.error(`Genome info not found in header ${this.getLocus()}. No fetch allowed.`);
        reject(Error(`Genome info not found in header ${this.getLocus()}. No fetch allowed.`));
      }
    });
  }

  /**
   * Returns header at appropriate version
   *
   * @param {number} version
   * @returns
   * @memberof BitkHeaderLegacy
   */
  public write(version: number) {
    const headerSpecs = this.getVersionSpecs(version);
    return headerSpecs.write(this.info);
  }

  /**
   * Returns a boolean to indicate if the gene info exists or not.
   *
   * @protected
   * @returns {boolean}
   * @memberof BitkHeaderLegacy
   */
  protected hasGeneInfo(): boolean {
    return this.info.mistInfo.stable_id !== '';
  }

  /**
   * Return an instance of the HeaderVersion of a given version
   *
   * @protected
   * @param {number} version
   * @returns {VersionSpecificationAbstract}
   * @memberof BitkHeaderLegacy
   */
  protected getVersionSpecs(version: number): VersionSpecificationAbstract {
    const versionSpecs = supportedVersions.find(supportedVersion => {
      const headeVersion = new supportedVersion();
      return headeVersion.version === version;
    });
    if (!versionSpecs) {
      throw new Error('Version ${version} has no specifications defined');
    }
    return new versionSpecs();
  }

  /**
   * Returns a guess of what version the original header is.
   *
   * @protected
   * @returns
   * @memberof BitkHeaderLegacy
   */
  protected sniffVersion() {
    const log = this.logging.getLogger('BitkHeaderLegacy::sniffVersion');
    const matches: number[] = [];
    const originalHeader = this.originalHeader as string;
    supportedVersions.forEach(versionSpecs => {
      const headerSpec = new versionSpecs(this.logLevel);
      const snifMatch = originalHeader.match(headerSpec.sniffer);
      if (snifMatch) {
        log.debug(snifMatch);
        matches.push(headerSpec.version);
      }
    });
    if (matches.length) {
      return Math.max(...matches);
    }
  }

  /*
		
		else if (this.originalVersion === 3) {
			const versionSpec = this.getVersionSpecs_(this.originalVersion)
			const fields = this.originalHeader.split(versionSpec.sep.header)
			const genReg = new RegExp(/.{2}/)
			const speReg = new RegExp('\\' + versionSpec.sep.genome + '.{3}')
			const orgID = fields[0]
			this.info.ge = orgID.match(genReg)[0]
			this.info.sp = orgID.match(speReg)[0].slice(1)
			const mist3Info = fields[1].split(versionSpec.sep.genomeVersion)
			this.info.geneInfo.stable_id = fields[1]
			this.info.genomeVersion = mist3Info[0]
			this.info.locus = mist3Info[1]
			this.isParsed = true
		}
		return this
	}



	getGenomeVersion() {
		if (this.hasGeneInfo())
			return this.info.geneInfo.stable_id.split('-')[0]
		return null
	}



	getAccession() {
		let accession = null
		if (([1, 2].indexOf(this.originalVersion)) !== -1)
			accession = this.info.accession
		else
			log.warn('This type of header does not have accessions')
		return accession
	}

	getGId() {
		let gid = null
		if (([1, 2].indexOf(this.originalVersion)) !== -1)
			gid = this.info.gid
		else
			log.warn('This type of header does not have MiST2 ids')
		return gid
	}

	getOrgId(ver) {
		const version = ver || this.originalVersion
		let orgId = null
		const versionSpec = this.getVersionSpecs_(version)
		const sep = versionSpec.sep
		if (([1, 2].indexOf(version)) !== -1)
			orgId = this.info.ge + sep.genome + this.info.sp + sep.genome + this.info.gid
		else if (version === 3)
			orgId = this.info.ge + sep.genome + this.info.sp
		else
			log.warn('This type of header does not have MiST2 ids')
		return orgId
	}

	getOriginalVersion() {
		return this.originalVersion
	}



	getOriginalHeader() {
		return this.originalHeader
	}

	// change this to get the info from extraInfo if available.
	toVersion(version, options = {force: false}) {
		this.isItParsed()
		const ver = version || this.originalVersion
		let header = ''
		const versionSpec = this.getVersionSpecs_(ver)
		log.debug(`version: ${ver}`)
		log.debug(this.info)
		switch (ver) {
			case 1: {
				const sep = versionSpec.sep
				log.debug(this.info)
				header = this.info.ge + sep.genome + this.info.sp + sep.genome + this.info.gid + sep.header + this.info.locus + sep.header + this.info.accession
				if (this.info.extra.length !== 0)
					header += sep.header + this.info.extra.join(sep.header)
				log.debug(`Header version 1 was build as: ${header}`)
				break
			}
			case 2: {
				const sep = versionSpec.sep
				log.debug(this.info)
				header = this.info.ge + sep.genome + this.info.sp + sep.genome + this.info.gid + sep.header + this.info.locus + sep.header + this.info.accession
				if (this.info.extra.length !== 0)
					header += sep.header + this.info.extra.join(sep.header)
				log.debug(`Header version 2 was build as: ${header}`)
				break
			}
			case 3: {
				const sep = versionSpec.sep
				if (!this.hasGeneInfo()) {
					log.error('Header version 3 can not be built without extra gene info')
					throw Error('Header version 3 can not be built without extra gene info')
				}
				header = this.info.ge + sep.genome + this.info.sp + sep.header + this.info.geneInfo.stable_id
				if (this.info.extra.length !== 0)
					header += sep.header + this.info.extra.join(sep.header)
				log.debug(`Header version 3 was build as: ${header}`)
				break
			}
		}
		return header
	}
	*/
}

export { BitkHeaderLegacy };
