import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { IHeaderInfo, IVersionSpecs, VersionSpecificationAbstract } from './VersionSpecificationsAbstract';

const specs: IVersionSpecs = {
  extraPos: 3,
  separators: {
    header: '-',
    oid: '.',
  },
  sniffer: new RegExp(
    /(^[a-zA-Z][a-z]|[A-Z][A-Z])\.([a-z]{2}\.|[a-z]{3}|[A-Z][a-z]{2}\.|\[[A-z][a-z]|'[A-Z][a-z]|[A-Z][a-z][a-z])\.[0-9]{1,6}-.*-([A-Z]{2,3}.[0-9]{5,10}.[0-9]{1}|REF_.*:.*).*/,
  ),
  version: 1,
};

class HeaderVersionOne extends VersionSpecificationAbstract {
  protected logLevel: LogLevelDescType;
  protected logging: Logger;

  constructor(logLevel: LogLevelDescType = 'info') {
    super(specs);
    this.logLevel = logLevel;
    this.logging = new Logger(logLevel);
  }

  public parse(header: string): IHeaderInfo {
    const log = this.logging.getLogger('HeaderVersionOne::parse');
    const info = {
      a: '',
      e: [] as string[],
      g: '',
      gid: '',
      l: '',
      mistInfo: {
        stable_id: '',
      },
      s: '',
    };
    const fields = header.split(this.separators.header);
    log.debug(fields);
    const orgID = fields[0];
    const genReg = new RegExp(/.{2}/);
    log.debug(`orgID parsed: ${orgID}`);

    const genRegMatch = orgID.match(genReg);
    if (!genRegMatch) {
      throw new Error(`Genus does not match version ${this.version} definition`);
    }
    info.g = genRegMatch[0];
    log.debug(`genusHumanId: ${info.g}`);

    const speReg = new RegExp('\\' + this.separators.oid + '.{3}');
    const speRegMatch = orgID.match(speReg);
    if (!speRegMatch) {
      throw new Error(`Species does not match version ${this.version} definition`);
    }
    info.s = speRegMatch[0].slice(1);
    log.debug(`speciesHumanId: ${info.s}`);

    const genIdReg = new RegExp(/[0-9]{1,4}/);
    const genIdRegMatch = orgID.match(genIdReg);
    if (!genIdRegMatch) {
      throw new Error(`Genome ID does not match version ${this.version} definition`);
    }
    info.gid = genIdRegMatch[0];
    log.debug(`genomeId: ${info.gid}`);

    info.l = fields[1];
    info.a = fields[2];
    info.e = fields.slice(this.extraPos);

    return info;
  }

  public write(info: IHeaderInfo): string {
    const oid = [info.g, info.s, info.gid].join(this.separators.oid);
    return [oid, info.l, info.a].concat(info.e).join(this.separators.header);
  }
}

export { HeaderVersionOne };
