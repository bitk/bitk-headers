// tslint:disable: no-unused-expression
import chai from 'chai'
import chaiAsPromised from 'chai-as-promised';

chai.use(chaiAsPromised)

const expect = chai.expect
const should = chai.should()

import { BitkHeader } from '../src/BitkHeader'

const logLevel = 'silent'


const fixtures = [
  {
    header: 'My_xan|GCF_000012685.1-MXAN_RS12980',
    info: { 
      e: [],
			g: 'My',
			gv: 'GCF_000012685.1',
      l: 'MXAN_RS12980',
      mistInfo: {
        stable_id: 'GCF_000012685.1-MXAN_RS12980'
      },
      s: 'xan' 
    },
    version: 3
  }
]

const badFixtures = [
	{
    header: 'My_xan_508|MXAN_2680|YP_630897.1',
    info: { 
      a: 'YP_630897.1',
      e: [],
      g: 'My',
      gid: '508',
      l: 'MXAN_2680',
      mistInfo: {
        stable_id: ''
      },
      s: 'xan' 
    },
    version: 2
  },
]

describe('BitkHeader', () => {
  describe('protected sniffVersion()', () => {
    class MockBitkHeader extends BitkHeader {
      public pubSniff() {
        return this.sniffVersion()
      }
    }
    it('should find the right version headers', () => {
      fixtures.forEach((fixture) => {
        const bitkHeader = new MockBitkHeader(logLevel)
        bitkHeader.parse(fixture.header)
        const ver = bitkHeader.pubSniff()
        expect(ver).eql(fixture.version)
      })
    })
  })
  describe('parse', () => {
    it('should get the right info', () => {
      fixtures.forEach((fixture) => {
        const bitkHeader = new BitkHeader(logLevel)
        bitkHeader.parse(fixture.header)
        const info = bitkHeader.getInfo()
        expect(info).eql(fixture.info)
      })
    })
  })
  describe('getLocus', ()=> {
		it('should give the locus in version 3', ()=> {
			const header = 'My_xan|GCF_000012685.1-MXAN_RS12980'
			const bitkHeader = new BitkHeader(logLevel)
			bitkHeader.parse(header)
			const expectedLocus = 'MXAN_RS12980'
			const locus = bitkHeader.getLocus()
			expect(locus).eq(expectedLocus)
		})
	})
  describe('fetchGeneInfo', () => {
		it('should work with header with version 3', async function() {
			this.timeout(10000)
			const header = 'Gr_hol|GCF_001558255.2-AL542_RS03615'
			const bitkHeader = new BitkHeader(logLevel)
			bitkHeader.parse(header)
			const expected = 'AL542_RS03615'
			await bitkHeader.fetchGeneInfo();
			const geneInfo = bitkHeader.getInfo();
			expect(geneInfo.mistInfo.locus).eql(expected);
		})
  })
  describe('write', () => {
		it('to versions 3', () => {
			fixtures.forEach((fixture) => {
				const bitkHeader = new BitkHeader(logLevel)
				bitkHeader.parse(fixture.header)
				const newHeader = bitkHeader.write(fixture.version)
				expect(newHeader).equal(fixture.header)
			})
    })
  })
  describe('addExtra', () => {
    it('should add extra information to header', () => {
      const fixs = [
        {
          extra: [
            'F3',
            'LJ'
          ],
          in: 'My_xan|GCF_000012685.1-MXAN_RS12980',
          out: 'My_xan|GCF_000012685.1-MXAN_RS12980|F3|LJ',
          outVer: 3
        }
      ] 
      fixs.forEach((fixture) => {
				const bitkHeader = new BitkHeader(logLevel)
				const newHeader = bitkHeader.parse(fixture.in).addExtra(fixture.extra).write(fixture.outVer)
				expect(newHeader).equal(fixture.out)
			})
    })
  })
})
