// tslint:disable: no-unused-expression
import chai from 'chai'
import chaiAsPromised from 'chai-as-promised';

chai.use(chaiAsPromised)

const expect = chai.expect
const should = chai.should()

import { BitkHeaderLegacy } from '../src/BitkHeaderLegacy'

const logLevel = 'silent'


const fixtures = [
  {
    header: 'My.xan.508-MXAN_2680-YP_630897.1',
    info: { 
      a: 'YP_630897.1',
      e: [],
      g: 'My',
      gid: '508',
      l: 'MXAN_2680',
      mistInfo: {
        stable_id: ''
      },
      s: 'xan' 
    },
    version: 1
  },
  {
    header: 'My_xan_508|MXAN_2680|YP_630897.1',
    info: { 
      a: 'YP_630897.1',
      e: [],
      g: 'My',
      gid: '508',
      l: 'MXAN_2680',
      mistInfo: {
        stable_id: ''
      },
      s: 'xan' 
    },
    version: 2
  },
  {
    header: 'Ca_Sym_7859|Cenrod_0332|REF_UCNUF:Cenrod_0332|F8|REF_UCNUF:Cenrod_0332',
    info: {
      a: 'REF_UCNUF:Cenrod_0332',
      e: [
        'F8',
        'REF_UCNUF:Cenrod_0332'
      ],
      g: 'Ca',
      gid: '7859',
      l: 'Cenrod_0332',
      mistInfo: {
        stable_id: ''
      },
      s: 'Sym'
    },
    version: 2
  }
]

describe('BitkHeaderLegacy', () => {
  describe('protected sniffVersion()', () => {
    class MockBitkHeaderLegacy extends BitkHeaderLegacy {
      public pubSniff() {
        return this.sniffVersion()
      }
    }
    it('should find the right version headers', () => {
      fixtures.forEach((fixture) => {
        const bitkHeader = new MockBitkHeaderLegacy(logLevel)
        bitkHeader.parse(fixture.header)
        const ver = bitkHeader.pubSniff()
        expect(ver).eql(fixture.version)
      })
    })
  })
  describe('parse', () => {
    it('should get the right info', () => {
      fixtures.forEach((fixture) => {
        const bitkHeader = new BitkHeaderLegacy(logLevel)
        bitkHeader.parse(fixture.header)
        const info = bitkHeader.getInfo()
        expect(info).eql(fixture.info)
      })
    })
  })
  describe('getLocus', ()=> {
		it('should give the locus in version 2', ()=> {
			const header = 'My_xan_508|MXAN_2680|YP_630897.1'
			const bitkHeader = new BitkHeaderLegacy(logLevel)
			bitkHeader.parse(header)
			const expectedLocus = 'MXAN_2680'
			const locus = bitkHeader.getLocus()
			expect(locus).eq(expectedLocus)
		})
		it('should give the locus in version 2', ()=> {
			const header = 'My.xan.508-MXAN_2680-YP_630897.1'

			const bitkHeader = new BitkHeaderLegacy(logLevel)
			bitkHeader.parse(header)
			const expectedLocus = 'MXAN_2680'
			const locus = bitkHeader.getLocus()
			expect(locus).eq(expectedLocus)
		})
	})
  describe('fetchGeneInfo', () => {
		it.skip('should work with header with version 3', () => {
			const header = 'Gr_hol|GCF_001558255.2-AL542_RS03615'
			const bitkHeader = new BitkHeaderLegacy(logLevel)
			bitkHeader.parse(header)
			const expected = 'GCF_001558255.2'
			return bitkHeader.fetchGeneInfo().then(() => {
				const genomeVersion = bitkHeader.getGenomeVersion()
				expect(genomeVersion).eql(expected)
			})
		})
		it('should also work with valid header without genome version (bitk version 1)', () => {
			const header = 'My.xan.508-MXAN_2680-YP_630897.1'
			const bitkHeader = new BitkHeaderLegacy(logLevel)
			bitkHeader.parse(header)
			return bitkHeader.fetchGeneInfo({fetch: false}).should.be.rejectedWith('Genome info not found in header MXAN_2680. No fetch allowed.')
		})
		it('should not work with valid header without genome version (bitk version 1)', () => {
			const header = 'My.xan.508-MXAN_2680-YP_630897.1'
			const bitkHeader = new BitkHeaderLegacy(logLevel)
			bitkHeader.parse(header)
			const expected = 'GCF_000012685.1'
			return bitkHeader.fetchGeneInfo().then(() => {
				const genomeVersion = bitkHeader.getGenomeVersion()
				expect(genomeVersion).eql(expected)
			})
		})
  })
  describe('write', () => {
		it('to versions 2', () => {
			const fixs = [
				{
					in: 'My.xan.508-MXAN_2680-YP_630897.1',
					out: 'My_xan_508|MXAN_2680|YP_630897.1',
					outVer: 2
        },
        {
					in: 'My_xan_508|MXAN_2680|YP_630897.1',
					out: 'My_xan_508|MXAN_2680|YP_630897.1',
					outVer: 2
				},
				{
					in: 'My_xan_508|MXAN_2680|YP_630897.1|F3|LJ',
					out: 'My_xan_508|MXAN_2680|YP_630897.1|F3|LJ',
					outVer: 2
        },
        {
					in: 'My.xan.508-MXAN_2680-YP_630897.1-F3-LJ',
					out: 'My_xan_508|MXAN_2680|YP_630897.1|F3|LJ',
					outVer: 2
				}
			]
			fixs.forEach((fixture) => {
				const bitkHeader = new BitkHeaderLegacy(logLevel)
				bitkHeader.parse(fixture.in)
				const newHeader = bitkHeader.write(fixture.outVer)
				expect(newHeader).equal(fixture.out)
			})
    })
    it('to versions 1', () => {
			const fixs = [
				{
          in: 'My_xan_508|MXAN_2680|YP_630897.1',
					out: 'My.xan.508-MXAN_2680-YP_630897.1',
					outVer: 1,
        },
        {
          in: 'My.xan.508-MXAN_2680-YP_630897.1',
					out: 'My.xan.508-MXAN_2680-YP_630897.1',
					outVer: 1,
				},
				{
          in: 'My.xan.508-MXAN_2680-YP_630897.1-F3-KH',
					out: 'My.xan.508-MXAN_2680-YP_630897.1-F3-KH',
					outVer: 1,
        },
        {
          in: 'My_xan_508|MXAN_2680|YP_630897.1|F3|KH',
          out: 'My.xan.508-MXAN_2680-YP_630897.1-F3-KH',
					outVer: 1,
				}
			]
			fixs.forEach((fixture) => {
				const bitkHeader = new BitkHeaderLegacy(logLevel)
				bitkHeader.parse(fixture.in)
				const newHeader = bitkHeader.write(fixture.outVer)
				expect(newHeader).equal(fixture.out)
			})
		})
		/*it.skip('Translate to versions 1 to 3 should throw', () => {
			let fixture = {
				in: 'My.xan.508-MXAN_2680-YP_630897.1',
				out: 'My_xan|GCF_000012685.1-MXAN_RS12980',
				outVer: 3
			}
			const bitkHeader = new BitkHeaderLegacy(fixture.in)
			bitkHeader.parse()
			expect(() => {
				return bitkHeader.toVersion(fixture.outVer)
			}).throw('Header version 3 can not be built without extra gene info')
		})
		it.skip('Translate to versions 1 to 3 should throw', () => {
			let fixture = {
				in: 'My.xan.508-MXAN_2680-YP_630897.1-F3-LJ',
				out: 'My_xan|GCF_000012685.1-MXAN_RS12980|F3|LJ',
				outVer: 3
			}
			const bitkHeader = new BitkHeaderLegacy(fixture.in)
			bitkHeader.parse()
			expect(() => {
				return bitkHeader.toVersion(fixture.outVer)
			}).throw('Header version 3 can not be built without extra gene info')
		})
		it.skip('Translate to versions 1 to 3 should pass if gene Info is fetched', () => {
			let fixture = {
				in: 'My.xan.508-MXAN_2680-YP_630897.1',
				out: 'My_xan|GCF_000012685.1-MXAN_RS12980',
				outVer: 3
			}
			const bitkHeader = new BitkHeaderLegacy(fixture.in)
			bitkHeader.parse()
			return bitkHeader.fetchGeneInfo().then((geneInfo) => {
				const newHeader = bitkHeader.toVersion(fixture.outVer)
				return expect(newHeader).equal(fixture.out)
			})
		})
		it.skip('Translate to versions 1 to 3 should pass if gene Info is fetched with extra info', () => {
			let fixture = 				{
				in: 'My.xan.508-MXAN_2680-YP_630897.1',
				out: 'My_xan|GCF_000012685.1-MXAN_RS12980',
				outVer: 3
			}
			const bitkHeader = new BitkHeaderLegacy(fixture.in)
			bitkHeader.parse()
			return bitkHeader.fetchGeneInfo().then((geneInfo) => {
				const newHeader = bitkHeader.toVersion(fixture.outVer)
				return expect(newHeader).equal(fixture.out)
			})
		})
		it.skip('Translate to versions 2 to 3 should throw', () => {
			let fixture = {
				in: 'My_xan_508|MXAN_2680|YP_630897.1',
				out: 'My_xan|GCF_000012685.1-MXAN_RS12980',
				outVer: 3
			}
			const bitkHeader = new BitkHeaderLegacy(fixture.in)
			bitkHeader.parse()
			expect(() => {
				return bitkHeader.toVersion(fixture.outVer)
			}).throw('Header version 3 can not be built without extra gene info')
		})
		it.skip('Translate to versions 2 to 3 should throw', () => {
			let fixture = {
				in: 'My_xan_508|MXAN_2680|YP_630897.1-F3-LJ',
				out: 'My_xan|GCF_000012685.1-MXAN_RS12980|F3|LJ',
				outVer: 3
			}
			const bitkHeader = new BitkHeaderLegacy(fixture.in)
			bitkHeader.parse()
			expect(() => {
				return bitkHeader.toVersion(fixture.outVer)
			}).throw('Header version 3 can not be built without extra gene info')
		})
		it.skip('Translate to versions 2 to 3 should pass if gene Info is fetched', () => {
			let fixture = {
				in: 'My_xan_508|MXAN_2680|YP_630897.1',
				out: 'My_xan|GCF_000012685.1-MXAN_RS12980',
				outVer: 3
			}
			const bitkHeader = new BitkHeaderLegacy(fixture.in)
			bitkHeader.parse()
			return bitkHeader.fetchGeneInfo().then((geneInfo) => {
				const newHeader = bitkHeader.toVersion(fixture.outVer)
				return expect(newHeader).equal(fixture.out)
			})
		})
		it.skip('Translate to versions 2 to 3 should pass if gene Info is fetched with extra info', () => {
			let fixture = 				{
				in: 'My_xan_508|MXAN_2680|YP_630897.1',
				out: 'My_xan|GCF_000012685.1-MXAN_RS12980',
				outVer: 3
			}
			const bitkHeader = new BitkHeaderLegacy(fixture.in)
			bitkHeader.parse()
			return bitkHeader.fetchGeneInfo().then((geneInfo) => {
				const newHeader = bitkHeader.toVersion(fixture.outVer)
				return expect(newHeader).equal(fixture.out)
			})
		}) */
  })
  describe('addExtra', () => {
    it('should add extra information to header', () => {
      const fixs = [
        {
          extra: [
            'F3',
            'LJ'
          ],
          in: 'My.xan.508-MXAN_2680-YP_630897.1',
          out: 'My_xan_508|MXAN_2680|YP_630897.1|F3|LJ',
          outVer: 2
        }
      ] 
      fixs.forEach((fixture) => {
				const bitkHeader = new BitkHeaderLegacy(logLevel)
				const newHeader = bitkHeader.parse(fixture.in).addExtra(fixture.extra).write(fixture.outVer)
				expect(newHeader).equal(fixture.out)
			})
    })
  })
})
