# bitk-headers

![npm](https://img.shields.io/npm/v/bitk-headers.svg)
![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-blue.svg)
[![pipeline status](https://gitlab.com/bitk/bitk-headers/badges/master/pipeline.svg)](https://gitlab.com/bitk/bitk-headers/commits/master)
[![coverage report](https://gitlab.com/bitk/bitk-headers/badges/master/coverage.svg)](https://bitk.gitlab.io/bitk-headers/coverage)

A library to support headers compatible with BITK's standard. Written in TypeScript.

# Install

```shell
npm install bitk-headers
```

# Versions

## BitkHeader version 3

The newest version of BitkHeaders follows the implementation of stable genes ids from [MiST3](mistdb.com). This version cannot be translated to Version 1 or 2 (see below) because it does not have the legacy MiST2 genome ids. However, versions 1 and 2 can be translated to this version with access to the MiST3 API.

Version 3: `Xx_yyy`|`genome_ncbi_version`-`locus`|extra fields`

Example: `My_xan|GCF_000012685.1-MXAN_RS12980`

|item | Description |
|:-|:-|
|`Xx` | First two letter of genus
|`yyy`| First three letters of species
|`genome_ncbi_version` | NCBI genome version code
|`locus`| Locus number of the gene
|`extra fields` | any other annotation needed: classifications and etc.

## BitkHeaderLegacy version 1 and 2

Version 1: `Xx.yyy.NNN-`_`locus`_`-`_`accession`_`-`_`extra fields`  
Example: `My.xan.508-MXAN_2680-YP_630897.1`  
Version 2: `Xx_yyy_NNN|`_`locus`_`|`_`accession`_`|`_`extra fields`  
Example: `My.xan.508-MXAN_2680-YP_630897.1`  

|item | Description |
|:-|:-|
|`Xx` | First two letter of genus
|`yyy`| First three letters of species
|`NNN`| MiST2 genome id
|`locus`| Locus number of the gene
|`accession` | NCBI accession number
|`extra fields` | any other annotation needed: classifications and etc.

# [Developer's Documentation](https://bitk.gitlab.io/bitk-headers/)

... to be continued.

Written with ❤ in Typescript.
